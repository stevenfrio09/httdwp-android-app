package com.frio.httdwpcontrolcenter.Utils;

public class FirebaseKeys {
    public static String UID = "";
    public static String batteryCapacity = "battery_capacity";
    public static String flowRate = "flow_rate";
    public static String voltage = "voltage";
    public static String status_internal = "charging_internal_battery";
    public static String status_discharging = "discharging";

    public static String relay = "relay";
    public static String relayStatus = "relay_status";

    public static String arduino = "arduino";
    public static String moduleStatus = "module_status";
    public static String total_wh = "total_wh";

}
