


package com.frio.httdwpcontrolcenter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.agik.AGIKSwipeButton.Controller.OnSwipeCompleteListener;
import com.agik.AGIKSwipeButton.View.Swipe_Button_View;
import com.facebook.login.LoginManager;
import com.frio.httdwpcontrolcenter.Utils.FirebaseKeys;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivity extends AppCompatActivity {


    FirebaseDatabase firebaseDatabase;
    int lastClickTime = 0;
    int onStartSeekBarValue = 0;
    TextView tV_battCapacity_content, tV_voltage_content, tV_flowrate_content, tv_chargingStatus_internal, tV_chargingStatus_discharging, tV_chargingStatus_disconnected, tV_chargingStatus_00, tV_chargingStatus_01, tV_chargingStatus_10, tV_chargingStatus_11;
    ConstraintLayout cl_main;
    Swipe_Button_View swipeBarRelay, swipeBarArduino;
        ImageView iv_battery_capacity;
    ProgressDialog progressDialog;
    ImageView cv_calc_icon;
    private DatabaseReference databaseReference;

    String arduinoStatus;
    String relayStatus, initSlideButtonRelayStatus, initSlideButtonArduinoStatus;

    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        tV_chargingStatus_00 = findViewById(R.id.tV_chargingStatus_00);
        tV_chargingStatus_01 = findViewById(R.id.tV_chargingStatus_01);
        tV_chargingStatus_10 = findViewById(R.id.tV_chargingStatus_10);
        tV_chargingStatus_11 = findViewById(R.id.tV_chargingStatus_11);
        cv_calc_icon = findViewById(R.id.cv_calc_icon);

        cl_main = findViewById(R.id.cl_main);



        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseKeys.UID = firebaseUser.getUid();
        //firebaseUserEmail.replace(".", "__DOT__");


        tV_battCapacity_content = findViewById(R.id.tV_battCapacity_content);
        tV_voltage_content = findViewById(R.id.tV_voltage_content);
        tV_flowrate_content = findViewById(R.id.tV_flowrate_content);
        iv_battery_capacity = findViewById(R.id.iv_battery_capacity);

        swipeBarRelay = findViewById(R.id.swipeBar_relay);
        swipeBarArduino = findViewById(R.id.swipeBar_arduino);


        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait.");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

//

//        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
//        connectedRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot snapshot) {
//                boolean connected = snapshot.getValue(Boolean.class);
//                if (connected) {
//                    Log.d("LOG", "connected");
//                } else {
//                    Log.d("LOG", "not connected");
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                System.err.println("Listener was cancelled");
//            }
//        });


        initSlideButtonValues();
        swipeBarListeners(swipeBarRelay, "relay");
        swipeBarListeners(swipeBarArduino, "arduino");
        getDataFromFirebase();


        cv_calc_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Usage.class));
            }
        });
    }



    private void initSlideButtonValues() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                int index = (int) snapshot.child(FirebaseKeys.UID).getChildrenCount() - 1;


                initSlideButtonArduinoStatus = String.valueOf(snapshot.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.moduleStatus).getValue());
                initSlideButtonRelayStatus = String.valueOf(snapshot.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.relayStatus).getValue());

                //progressDialog.show();



                        if (initSlideButtonArduinoStatus.equals("ON")) {

                            swipeBarArduino.setreverse_180();
                            swipeBarArduino.setThumbImage(getResources().getDrawable(R.drawable.ic_slider_on));


                        } else if (initSlideButtonArduinoStatus.equals("OFF")) {


                            swipeBarArduino.setreverse_0();
                            swipeBarArduino.setThumbImage(getResources().getDrawable(R.drawable.ic_slider_off));
                        }

                        if (initSlideButtonRelayStatus.equals("ON")) {

                            swipeBarRelay.setreverse_180();
                            swipeBarRelay.setThumbImage(getResources().getDrawable(R.drawable.ic_slider_on));


                        } else if (initSlideButtonRelayStatus.equals("OFF")) {


                            swipeBarRelay.setreverse_0();
                            swipeBarRelay.setThumbImage(getResources().getDrawable(R.drawable.ic_slider_off));
                        }

                        progressDialog.dismiss();



            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this, "An error has occurred.", Toast.LENGTH_SHORT);
            }
        });
    }

    private void swipeBarListeners(Swipe_Button_View swipeBar, String value) {

        swipeBar.setOnSwipeCompleteListener_forward_reverse(new OnSwipeCompleteListener() {
            @Override
            public void onSwipe_Forward(Swipe_Button_View swipe_button_view) {

                // if swipe forward is detected, set button to turned off state
//                if (swipe_button_view.swipe_both_direction) {
                swipe_button_view.setreverse_180();
                swipe_button_view.setThumbImage(getResources().getDrawable(R.drawable.ic_slider_on));
                //Snackbar.make(cl_main, "Unable to remotely turn on the device", Snackbar.LENGTH_SHORT).show();
                //Log.d("log", "blablbalba");

                Log.d("TAG", "onSwipeForward called");

                if(value.equals("arduino")){
                    arduinoStatus = "ON";
                    setArduinoStatus(arduinoStatus);

                }else{
                    relayStatus = "ON";
                    setRelayStatus(relayStatus);
                }
                //}
            }

            @Override
            public void onSwipe_Reverse(Swipe_Button_View swipe_button_view) {

                swipe_button_view.setreverse_0();
                swipe_button_view.setThumbImage(getResources().getDrawable(R.drawable.ic_slider_off));
                Log.d("TAG", "onSwipeReverseCalled");
                if(value.equals("arduino")) {
                    arduinoStatus = "OFF";
                    setArduinoStatus(arduinoStatus);
                }else{
                    relayStatus = "OFF";
                    setRelayStatus(relayStatus);
                }
            }
        });

    }

    private void setArduinoStatus(String value) {
        progressDialog.show();
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        int index = (int) snapshot.child(FirebaseKeys.UID).getChildrenCount() - 1;


                        String swipeBarStatus = String.valueOf(snapshot.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.moduleStatus).getValue());



                        if (!swipeBarStatus.equals(value)) {

                            databaseReference.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.moduleStatus).setValue(value, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                                    if (error != null) {

                                        progressDialog.dismiss();

                                        Toast.makeText(MainActivity.this, "An error has occurred", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                        progressDialog.dismiss();
                    }
                }, 1000);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "An error has occurred", Toast.LENGTH_SHORT);
            }
        });
    }

    private void setRelayStatus(String value) {
        progressDialog.show();
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        int index = (int) snapshot.child(FirebaseKeys.UID).getChildrenCount() - 1;


                        String swipeBarStatus = String.valueOf(snapshot.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.relayStatus).getValue());

                        if (!swipeBarStatus.equals(value)) {

                            databaseReference.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.relayStatus).setValue(value, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                                    if (error != null) {

                                        progressDialog.dismiss();

                                        Toast.makeText(MainActivity.this, "An error has occurred", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                        progressDialog.dismiss();
                    }
                }, 1000);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "An error has occurred", Toast.LENGTH_SHORT);
            }
        });

    }


    private void getDataFromFirebase() {

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                int index = (int) snapshot.child(FirebaseKeys.UID).getChildrenCount() - 1;

                Log.d("log", "index: " + index);

                String battery_capacity = String.valueOf(snapshot.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.batteryCapacity).getValue());
                String flow_rate = String.valueOf(snapshot.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.flowRate).getValue());
                String voltage = String.valueOf(snapshot.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.voltage).getValue());
                String status_internal = String.valueOf(snapshot.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.status_internal).getValue());
                String status_discharging = String.valueOf(snapshot.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.status_discharging).getValue());

                Log.d("log", "battery capacity : " + battery_capacity);
                Log.d("log", "flow rate : " + flow_rate);
                Log.d("log", "voltage : " + voltage);
                Log.d("log", "charging_internal : " + status_internal);
                Log.d("log", "discharging : " + status_discharging);


                tV_battCapacity_content.setText(battery_capacity);
                tV_flowrate_content.setText(flow_rate);

                if (status_internal.equals("false") && status_discharging.equals("false")) {
                    tV_chargingStatus_00.setVisibility(View.VISIBLE);
                    tV_chargingStatus_01.setVisibility(View.GONE);
                    tV_chargingStatus_10.setVisibility(View.GONE);
                    tV_chargingStatus_11.setVisibility(View.GONE);
                } else if(status_internal.equals("false") && status_discharging.equals("true")) {
                    tV_chargingStatus_00.setVisibility(View.GONE);
                    tV_chargingStatus_01.setVisibility(View.VISIBLE);
                    tV_chargingStatus_10.setVisibility(View.GONE);
                    tV_chargingStatus_11.setVisibility(View.GONE);
                } else if(status_internal.equals("true") && status_discharging.equals("false")){
                    tV_chargingStatus_00.setVisibility(View.GONE);
                    tV_chargingStatus_01.setVisibility(View.GONE);
                    tV_chargingStatus_10.setVisibility(View.VISIBLE);
                    tV_chargingStatus_11.setVisibility(View.GONE);
                }else if(status_internal.equals("true")&& status_discharging.equals("true")){
                    tV_chargingStatus_00.setVisibility(View.GONE);
                    tV_chargingStatus_01.setVisibility(View.GONE);
                    tV_chargingStatus_10.setVisibility(View.GONE);
                    tV_chargingStatus_11.setVisibility(View.VISIBLE);
                }

//                    if (status_internal.equals("false")) {
//                        tv_chargingStatus_internal.setVisibility(View.GONE);
//                        tV_chargingStatus_disconnected.setVisibility(View.GONE);
//                    } else {
//                        tv_chargingStatus_internal.setVisibility(View.VISIBLE);
//                    }
//
//
//                    if (status_discharging.equals("false")) {
//                        tV_chargingStatus_discharging.setVisibility(View.GONE);
//                        tV_chargingStatus_disconnected.setVisibility(View.GONE);
//                    } else {
//                        tV_chargingStatus_discharging.setVisibility(View.VISIBLE);
//                    }
                //}
                tV_voltage_content.setText(voltage);

                if(battery_capacity.equals("100")){
                    iv_battery_capacity.setImageResource(R.drawable.battery100);
                }else if(battery_capacity.equals("80")){
                    iv_battery_capacity.setImageResource(R.drawable.battery80);
                }else if(battery_capacity.equals("60")){
                    iv_battery_capacity.setImageResource(R.drawable.battery60);
                }else if(battery_capacity.equals("40")){
                    iv_battery_capacity.setImageResource(R.drawable.battery40);
                }else if(battery_capacity.equals("20")){
                    iv_battery_capacity.setImageResource(R.drawable.battery20);
                }else if(battery_capacity.equals("0")){
                    iv_battery_capacity.setImageResource(R.drawable.battery00);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Snackbar snackbar = Snackbar.make(cl_main, "Do you want to log out?", Snackbar.LENGTH_SHORT).setAction("Log out", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logOut();
                firebaseAuth.signOut();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        MainActivity.super.onBackPressed();
                    }
                },1000);
            }
        });

        snackbar.show();



    }

}