package com.frio.httdwpcontrolcenter;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.frio.httdwpcontrolcenter.Utils.FirebaseKeys;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Usage extends AppCompatActivity {


    ConstraintLayout cl_usage;
    TextInputEditText tet_kW, tet_price, tet_total;
    TextInputLayout til_kW, til_price, til_total;

    SwipeRefreshLayout srl_usage;
    CardView cv_calc;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    float kWh;
    float price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usage);


        cl_usage = findViewById(R.id.cl_usage);
        tet_kW = findViewById(R.id.tet_kWh);
        til_kW = findViewById(R.id.til_kWh);

        tet_price = findViewById(R.id.tet_price);
        til_price = findViewById(R.id.til_price);

        tet_total = findViewById(R.id.tet_total);
        til_total = findViewById(R.id.til_cost);

        cv_calc = findViewById(R.id.cv_calculate);
        srl_usage = findViewById(R.id.srl_usage);

        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        srl_usage.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getValueFromFirebase();

            }
        });



//
//        tet_price.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                kWh = Integer.parseInt(tet_kW.getText().toString());
//                try {
//                    multiplier(kWh, Integer.parseInt(String.valueOf(s)));
//                }catch (NumberFormatException e){
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                kWh = Integer.parseInt(tet_kW.getText().toString());
//                try {
//                    multiplier(kWh, Integer.parseInt(String.valueOf(s)));
//                }catch (NumberFormatException e){
//
//                }
//            }
//        });


        cv_calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    kWh = Float.parseFloat(tet_kW.getText().toString());
                    price = Float.parseFloat(tet_price.getText().toString());
                    multiplier(kWh, price);

                }catch (NumberFormatException e){

                }
            }
        });

    }


    private void multiplier(float digit1, float digit2){
        float firstDigit = digit1/1000;
        float total = firstDigit * digit2;
        tet_total.setText(String.valueOf(total));
    }


    @Override
    protected void onStart() {
        super.onStart();
        getValueFromFirebase();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getValueFromFirebase();
    }

    private void getValueFromFirebase(){
        srl_usage.isRefreshing();
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                int index = (int) snapshot.child(FirebaseKeys.UID).getChildrenCount() - 1;

                String swipeBarStatus = String.valueOf(snapshot.child(FirebaseKeys.UID).child(String.valueOf(index)).child(FirebaseKeys.total_wh).getValue());

                tet_kW.setText(swipeBarStatus);

                srl_usage.setRefreshing(false);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

                Toast.makeText(Usage.this, "An error has occurred", Toast.LENGTH_SHORT);
                srl_usage.setRefreshing(false);
            }
        });
    }
}