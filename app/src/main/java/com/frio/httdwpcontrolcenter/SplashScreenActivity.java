package com.frio.httdwpcontrolcenter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Timer;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoginStatusCallback;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashScreenActivity extends AppCompatActivity {

    Animation anim_fade_in_01, anim_translate, anim_fade_in_02;
    ImageView iv_layer_01,iv_layer_02, iv_layer_03;

    CallbackManager callbackManager;
    ConstraintLayout cl_splash;
    Snackbar snackbar;

    private static final String EMAIL = "email";
    private static final String TAG = "firebase_fb";
    LoginButton loginButton;
    private FirebaseAuth mAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen_acticvity);

        iv_layer_01 = findViewById(R.id.iv_layer_01);
        iv_layer_02 = findViewById(R.id.iv_layer_02);
        iv_layer_03 = findViewById(R.id.iv_layer_03);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        cl_splash = findViewById(R.id.cl_splash);
        snackbar = Snackbar.make(cl_splash, "Press back again to exit", Snackbar.LENGTH_SHORT);

        iv_layer_01.setAnimation(anim_translate);
        iv_layer_01.setVisibility(View.VISIBLE);

        anim_translate = AnimationUtils.loadAnimation(this, R.anim.anim_translate);
        anim_fade_in_01 = AnimationUtils.loadAnimation(this, R.anim.anim_fade_in);
        anim_fade_in_02 = AnimationUtils.loadAnimation(this, R.anim.anim_fade_in);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                iv_layer_02.setAnimation(anim_fade_in_01);
                iv_layer_02.setVisibility(View.VISIBLE);
                //iv_layer_03.setAnimation(anim_fade_in);


            }
        }, 1500);
//
//

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                iv_layer_03.setAnimation(anim_fade_in_02);
                iv_layer_03.setVisibility(View.VISIBLE);
            }
        }, 3000);


        callbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();

        loginButton.setReadPermissions("email", "public_profile");

        // If you are using in a fragment, call loginButton.setFragment(this);




        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Log.d(TAG, "facebookExceptionError:onCompleted");

                            String email = object.getString("email");
                            String name = object.getString("name");

                            Log.d(TAG, "email: " + email);
                            Log.d(TAG, "name: " + name);

                            handleFacebookAccessToken(loginResult.getAccessToken());



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });

                Bundle bundle = new Bundle();
                bundle.putString("fields", "email, name");
                graphRequest.setParameters(bundle);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebookExceptionError:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebookExceptionError:onError");
            }
        });

    }

    private void checkIfUserIsLoggedIn() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser!= null){

            updateUI();

        }else{
            Log.d(TAG, "firebaseUser == null");
        }
    }

    // ...
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            // Pass the activity result back to the Facebook SDK
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        checkIfUserIsLoggedIn();



    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            user.getEmail();
                            updateUI();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
//                            Toast.makeText(SplashScreenActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
//                            updateUI(null);
                        }

                        // ...
                    }
                });
    }
//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
//                finish();
//            }
//        }, 4500);

    private void updateUI(){
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {



        //snackbar.show();
        if(snackbar.isShown()){
            super.onBackPressed();
        }else{
            snackbar.show();
        }
    }
}